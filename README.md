# uname_spoofer
For when you really need to trick cpupower into thinking you're running the vanilla kernel.

## Problem
The Radeon Open Compute environment(ROCm) kernel from AMD doesn't provide a `linux-tools-<release>`, which will cause the `cpupower` utility to fail when it tries to lookup the particular cpupower implimentation for the installed kernel.

```
tsweeney@pantheon-0:~/uname_spoofer$ cpupower
WARNING: cpupower not found for kernel 4.11.0-kfd

  You may need to install the following packages for this specific kernel:
    linux-tools-4.11.0-kfd-compute-rocm-rel-1.6-180
    linux-cloud-tools-4.11.0-kfd-compute-rocm-rel-1.6-180

  You may also want to install one of the following packages to keep up to date:
    linux-tools-compute-rocm-rel-1.6-180
    linux-cloud-tools-compute-rocm-rel-1.6-180
```

It's worthwhile to note AMD should be expected to ship a `linux-tools-<release>` for their kernel, and this script should never have come into existance. Every time a system command is man-in-the-middled to introduce 'compatibility', a soul escapes from hell to haunt the lives of the innocent. 

## Usage
Uname_spoofer operates by being placed into `PATH` ahead of the directory in which the system `uname` lives (typically /bin/uname) so that it can intercept invokations. The spoofer will trasparently pass through all arguments to the system uname, and replace all instances of the kernel release in the output with the environment variable `SPOOF_UNAME`, if that environment variable is set. Uname's behaviour is unmodified, and will behave as expected with all options.

Note that you should never, ever put the uname spoofer permanently in your path. Hell will freeze over and your machine will disolve into a puddle of broken applications that check uname and see that your uname equals 'funky-test-string'. Instead, you should use the below syntax to add `SPOOF_UNAME` and modify `PATH`: 
```
SPOOF_UNAME="linux-generic" PATH="/path/to/uname_spoofer:$PATH" ./myapp arg1 --flag1=val1 ...
```
This will set environment variables for the child process that runs `myapp`, and save your system from the absymal nightmare of having your package managers and kernel-specific commands burst into flames.

To invoke cpupower and inject the spoofer to use the `generic` kernel, this invokation looks like:
```
SPOOF_UNAME="linux-generic" PATH="/path/to/uname_spoofer:$PATH" cpupower ...
```

## Epilogue
I'm so sorry that this exists. I wish it didn't, uname should never be intercepted. If you use this without being sure you need it, consider yourself formally warned your system may implode as all kernel dependencies implode in a ball of fire.
